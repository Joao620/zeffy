export default [
    {
        titulo: 'De mini a gigante',
        conteudo: 'Há raças de coelhos que, quando adultos, vão de 800 gramas, como o Netherland Dwarf, a mais de 10 quilos. Um exemplar da raça Continental Giant é o recordista, pesando mais de 20 quilos'
    },
    {
        titulo: 'Barulhinhos',
        conteudo: 'Em alguns momentos, o coelho pode emitir ruídos baixos, como o ronronar do gato, ou um tique-tique com os dentes, quando estão confortáveis. Em caso de dor ou raiva, podem soltar gritos agudos '
    },
    {
        titulo: '360 graus',
        conteudo: 'Como têm os olhos nas laterais da cabeça, os coelhos podem enxergar quase 360º à sua volta, mas não podem ver o que está bem em frente ao seu nariz. Para suprir essa deficiência, os bigodes funcionam como sensores de presença'
    },
    {
        titulo: 'Ligeirinho',
        conteudo: 'Perseguido, um coelho pode correr a mais de 50 quilômetros por hora e saltar mais de um metro de altura'
    },
    {
        titulo: 'Dentuço',
        conteudo: 'os dentes dos coelhos não param de crescer, por isso eles estão sempre mordendo ou roendo alguma coisa dura'
    },
    {
        titulo: 'Melhor ficar em casa',
        conteudo: 'A expectativa de vida de um coelho em estado selvagem é de quatro a cinco anos. Quando domesticado, pode ir de oito a dez anos'
    },
    {
        titulo: 'Signo de paz',
        conteudo: 'O coelho é um dos 12 signos da astrologia chinesa, sendo associado à sociabilidade, proteção e sentimentalismo'
    },
    {
        titulo: 'Baladeiro',
        conteudo: 'O coelho tem hábitos noturnos, passando a maior parte do dia na toca. Quem tem um coelho de estimação e passa o dia fora, quando chega em casa à noite encontra o bichinho em seu momento mais ativo para brincadeiras'
    },
    {
        titulo: 'Coelhos felizes',
        conteudo: 'Quando um coelho está feliz, não consegue esconder, pois são criaturas muito expressivas e mais ainda quando querem dar a entender que estão contentes'
    },
    {
        titulo: 'Eles não devem tomar banho',
        conteudo: 'Coelhos são animais muito limpos e eles tomam cuidado da sua higiene eles mesmos. Se por alguma razão, eles ficarem muito sujos, nós podemos lavar eles com shampoo seco ou com um pouco de água, prestando atenção para não fazer movimentos bruscos, para não assustá-los'
    },
    {
        titulo: 'Eles dormem muito',
        conteudo: 'Nós não devemos nos assustar se notamos que o nosso amigo peludo dorme muitas vezes. É de sua natureza tira cerca de vinte cochilos por dia. O engraçado, é que os olhos permanecem semi fechados, de modo que ele esteja sempre pronto para fugir em caso de perigo'
    },
    {
        titulo: 'Oi duda',
        conteudo: 'Então, eu só queria meio que agradecer pela a nossa amizade, principalmente quando eu tava sem são paulo, você era uma das únicas pessoas que eu conversava, e isso era legal, hoje você não é mais a única, mas eu ainda gosto muito de poder jogar uma horinha fora conversando contigo, s2'
    },

]